package de.uzer6.haw.sudoku.Controller;

import de.uzer6.haw.sudoku.Model.SudokuModel;
import de.uzer6.haw.sudoku.Solver.Constraint.BlockConstraint;
import de.uzer6.haw.sudoku.Solver.Constraint.ColumnConstraint;
import de.uzer6.haw.sudoku.Solver.Constraint.RowConstraint;
import de.uzer6.haw.sudoku.Solver.SudokuSolver;
import de.uzer6.haw.sudoku.View.ApplicationView;

/**
 * Created by lberger on 16/09/16.
 */
public class GameController {
    protected ApplicationView view;
    protected SudokuModel gameState;

    public GameController() {
        this.gameState = new SudokuModel();
        this.view = new ApplicationView(this);
    }

    public void loadBoard(SudokuModel board) {
        this.getView().getGameView().applySudokuModel(board, true);
    }

    public ApplicationView getView() {
        return this.view;
    }

    public void onViewChange() {
        this.view.getGameView().resetColors();
        for (int row = 0; row < 9; row++) {
            for (int column = 0; column < 9; column++) {
                try {
                    this.gameState.setField(row, column, this.view.getGameView().getFieldValue(row, column));
                } catch (IllegalArgumentException e) {
                    this.view.getGameView().markFieldInvalid(row, column);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }

        boolean isValid = true;
        for (int i = 0; i < 9; i++) {
            if (false == RowConstraint.isValidRow(this.gameState, i)) {
                this.view.getGameView().markRowInvalid(i);
                isValid = false;
            }
            if (false == ColumnConstraint.isValidColumn(this.gameState, i)) {
                this.view.getGameView().markColumnInvalid(i);
                isValid = false;
            }
        }
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                if (false == BlockConstraint.isValidBlock(this.gameState, row, column)) {
                    this.view.getGameView().markBoxInvalid(row, column);
                    isValid = false;
                }
            }
        }

        // No errors, check if whole board is filled
        if (isValid) {
            for (int row = 0; row < 9; row++) {
                for (int column = 0; column < 9; column++) {
                    Integer value = this.gameState.getField(row, column);
                    if (value == null) {
                        isValid = false;
                        break;
                    }
                }
                if (false == isValid) {
                    break;
                }
            }
        }

        // Whole board filled without errors => victory
        if (isValid) {
            this.view.getGameView().markBoardGreen();
        }
    }
}
