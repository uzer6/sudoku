package de.uzer6.haw.sudoku.View;

import de.uzer6.haw.sudoku.Controller.GameController;
import de.uzer6.haw.sudoku.Model.SudokuModel;

import javax.swing.*;
import java.awt.*;

/**
 * Created by lberger on 16/09/16.
 */
public class GameView extends JPanel {

    protected GameController controller;

    protected Block[][] boxes;

    protected Field[][] inputs;

    public GameView(GameController controller) {
        super(new GridLayout(3, 3));

        this.controller = controller;

        this.boxes = new Block[3][3];
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                this.boxes[row][column] = new Block(row, column);
                this.add(this.boxes[row][column]);
            }
        }

        // Initialize inputs
        this.inputs = new Field[9][9];
        for (int row = 0; row < 9; row++) {
            for (int column = 0; column < 9; column++) {
                this.inputs[row][column] = new Field(this.controller, column, row);

                // Sort input into pane
                this.boxes[(int) Math.floor(row / 3)][(int) Math.floor(column / 3)].add(this.inputs[row][column]);
            }
        }
    }

    /**
     * Updates given sudoku field
     * @param row
     * @param column
     * @param value
     * @throws IndexOutOfBoundsException
     * @throws IllegalArgumentException
     */
    public void updateField(int row, int column, Integer value) throws IndexOutOfBoundsException, IllegalArgumentException {
        if (row < 0 || row > 8 || column < 0 || column > 8) {
            throw new IndexOutOfBoundsException();
        }
        if (value != null && (value < 1 || value > 9)) {
            throw new IllegalArgumentException();
        }
        if (value != null) {
            this.inputs[row][column].setText(value.toString());
        } else {
            this.inputs[row][column].setText("");
        }
    }

    public void applySudokuModel(SudokuModel model) {
        this.applySudokuModel(model, false);
    }

    public void applySudokuModel(SudokuModel model, boolean lock) {
        for (int row = 0; row < 9; row++) {
            for (int column = 0; column < 9; column++) {
                Integer val = model.getField(row, column);
                this.updateField(row, column, val);
                if (lock && val != null) {
                    this.lockField(row, column);
                } else {
                    this.unlockField(row, column);
                }
            }
        }
    }

    public Integer getFieldValue(int row, int column) throws IndexOutOfBoundsException {
        if (row < 0 || row > 8 || column < 0 || column > 8) {
            throw new IndexOutOfBoundsException();
        }
        String value = this.inputs[row][column].getText();
        if (value.length() <= 0) {
            return null;
        }
        return Integer.parseInt(value);
    }

    public void markRowInvalid(int row) {
        for (int column = 0; column < 9; column++) {
            this.inputs[row][column].setBackground(Color.RED);
        }
    }

    public void markColumnInvalid(int column) {
        for (int row = 0; row < 9; row++) {
            this.inputs[row][column].setBackground(Color.RED);
        }
    }

    public void markBoxInvalid(int boxRow, int boxColumn) {
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                this.inputs[((boxRow * 3) + row)][((boxColumn * 3) + column)].setBackground(Color.RED);
            }
        }
    }

    public void markFieldInvalid(int x, int y) {
        this.inputs[x][y].setBackground(Color.RED);
    }

    public void markBoardGreen() {
        for (int row = 0; row < 9; row++) {
            for (int column = 0; column < 9; column++) {
                this.inputs[row][column].setBackground(Color.GREEN);
            }
        }
    }

    public void resetColors() {
        for (int row = 0; row < 9; row++) {
            for (int column = 0; column < 9; column++) {
                this.inputs[row][column].setBackground(Color.WHITE);
            }
        }
    }

    public void focusField(int row, int column) {
        if (this.inputs[row] != null && this.inputs[row][column] != null) {
            this.inputs[row][column].grabFocus();
        }
    }

    /**
     * Checkes if give field is locked
     * @param row
     * @param column
     * @return boolean
     */
    public boolean isLockedField(int row, int column) {
        return false == this.inputs[row][column].isEnabled();
    }

    /**
     * Lock given field
     * @param row
     * @param column
     */
    public void lockField(int row, int column) {
        this.inputs[row][column].setEnabled(false);
    }

    /**
     * Unlock given field
     * @param row
     * @param column
     */
    public void unlockField(int row, int column) {
        this.inputs[row][column].setEnabled(true);
    }
}
