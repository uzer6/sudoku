package de.uzer6.haw.sudoku.View;

import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import java.awt.*;

/**
 * Created by lberger on 16/09/16.
 */
public class Block extends JPanel {
    protected int row;
    protected int column;

    public Block(int column, int row) throws IndexOutOfBoundsException {
        super(new GridLayout(3, 3));
        this.setBorder(new BorderUIResource.LineBorderUIResource(Color.BLACK));
        this.column = column;
        this.row = row;
    }

    public int getRow() {
        return this.row;
    }

    protected void setRow(int row) throws IndexOutOfBoundsException {
        if (row < 0 || row > 8) {
            throw new IndexOutOfBoundsException();
        }

        this.row = row;
    }

    public int getColumn() {
        return this.column;
    }

    protected void setColumn(int column) throws IndexOutOfBoundsException {
        if (column < 0 || column > 8) {
            throw new IndexOutOfBoundsException();
        }

        this.column = column;
    }
}
