package de.uzer6.haw.sudoku.View;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class FieldDocument extends PlainDocument {

    protected int limit;

    public FieldDocument(int limit) {
        super();
        this.limit = limit;
    }

    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
        if (str == null) {
            return;
        }

        try {
            Integer intVal = Integer.parseInt(str);
            if ((this.getLength() + str.length()) <= limit && intVal > 0 && intVal <= 9) {
                super.insertString(offset, str, attr);
            }
        } catch (NumberFormatException e) {}

    }
}
