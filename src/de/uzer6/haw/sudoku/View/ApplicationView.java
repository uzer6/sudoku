package de.uzer6.haw.sudoku.View;

import de.uzer6.haw.sudoku.Controller.GameController;

import javax.swing.*;


/**
 * Created by lberger on 16/09/16.
 */
public class ApplicationView extends JFrame {
    protected GameController controller;

    protected GameView gameView;

    public ApplicationView(GameController controller) {
        super("Sudoku");
        // Frame setup
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 500);

        this.controller = controller;

        this.gameView = new GameView(this.controller);

        this.add(this.gameView);

        this.setVisible(true);
    }

    public GameView getGameView() {
        return this.gameView;
    }
}
