package de.uzer6.haw.sudoku.View;

import de.uzer6.haw.sudoku.Controller.GameController;
import de.uzer6.haw.sudoku.Listener.KeyboardListener;
import de.uzer6.haw.sudoku.Listener.FocusSelectListener;
import de.uzer6.haw.sudoku.Listener.ViewChangeListener;

import javax.swing.*;
import java.awt.*;

/**
 * Created by lberger on 16/09/16.
 */
public class Field extends JTextField {
    protected GameController controller;

    protected int row;
    protected int column;

    public Field(GameController controller, int column, int row) throws IndexOutOfBoundsException {
        super();

        this.controller = controller;

        this.setRow(row);
        this.setColumn(column);

        this.setHorizontalAlignment(SwingConstants.CENTER);
        this.setDisabledTextColor(Color.DARK_GRAY);

        // Limit to integer input 1-9 single digit
        this.setDocument(new FieldDocument(1));

        // Select value on focus
        this.addFocusListener(new FocusSelectListener(this));

        // Trigger gamestate update on change
        this.getDocument().addDocumentListener(new ViewChangeListener(this.controller));

        // Arrowkey navigation
        this.addKeyListener(new KeyboardListener(this.controller, this.getColumn(), this.getRow()));
    }

    public int getRow() {
        return this.row;
    }

    protected void setRow(int row) throws IndexOutOfBoundsException {
        if (row < 0 || row > 8) {
            throw new IndexOutOfBoundsException();
        }

        this.row = row;
    }

    public int getColumn() {
        return this.column;
    }

    protected void setColumn(int column) {
        if (column < 0 || column > 8) {
            throw new IndexOutOfBoundsException();
        }

        this.column = column;
    }
}
