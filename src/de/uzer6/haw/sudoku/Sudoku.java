package de.uzer6.haw.sudoku;

import de.uzer6.haw.sudoku.Controller.GameController;
import de.uzer6.haw.sudoku.Model.SudokuModel;
import de.uzer6.haw.sudoku.Solver.SudokuSolver;

/**
 * Created by lberger on 16/09/16.
 */
public class Sudoku {
    public static void main(String[] args) {
        GameController controller = new GameController();
        //controller.loadBoard(SudokuSolver.getSolvedGrid(new SudokuModel()));
        controller.loadBoard(Sudoku.getDebugBoard());
    }

    public static SudokuModel getDebugBoard() {
        SudokuModel board = new SudokuModel();

        board.setField(0, 1, new Integer(3));

        board.setField(1, 3, new Integer(1));
        board.setField(1, 4, new Integer(9));
        board.setField(1, 5, new Integer(5));

        board.setField(2, 2, new Integer(8));
        board.setField(2, 7, new Integer(6));

        board.setField(3, 0, new Integer(8));
        board.setField(3, 4, new Integer(6));

        board.setField(4, 0, new Integer(4));
        board.setField(4, 3, new Integer(8));
        board.setField(4, 8, new Integer(1));

        board.setField(5, 4, new Integer(2));

        board.setField(6, 1, new Integer(6));
        board.setField(6, 6, new Integer(2));
        board.setField(6, 7, new Integer(8));

        board.setField(7, 3, new Integer(4));
        board.setField(7, 4, new Integer(1));
        board.setField(7, 5, new Integer(9));
        board.setField(7, 8, new Integer(5));

        board.setField(8, 7, new Integer(7));

        return board;
    }
}
