package de.uzer6.haw.sudoku.Util;

import java.util.BitSet;

/**
 * Created by lberger on 17/09/16.
 */
public class SudokuUtil {
    private static SudokuUtil ourInstance = new SudokuUtil();

    public static SudokuUtil getInstance() {
        return ourInstance;
    }

    protected BitSet filledBoardMask;

    private SudokuUtil() {
    }

    public BitSet getFilledBoardMask() {
        if (this.filledBoardMask == null) {
            this.filledBoardMask = new BitSet(81);
            for (int i = 0; i < 81; i++) {
                this.filledBoardMask.set(i);
            }
        }

        return (BitSet) this.filledBoardMask.clone();
    }
}
