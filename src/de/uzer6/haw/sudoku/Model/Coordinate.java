package de.uzer6.haw.sudoku.Model;

/**
 * Created by lberger on 17/09/16.
 */
public class Coordinate {
    protected int row;
    protected int column;

    public Coordinate(int row, int column) throws IndexOutOfBoundsException {
        this.setRow(row);
        this.setColumn(column);
    }

    public int getRow() {
        return this.row;
    }

    public void setRow(int row) throws IndexOutOfBoundsException {
        if (row < 0 || row > 8) {
            throw new IndexOutOfBoundsException();
        }

        this.row = row;
    }

    public int getColumn() {
        return this.column;
    }

    public void setColumn(int column) throws IndexOutOfBoundsException {
        if (column < 0 || column > 8) {
            throw new IndexOutOfBoundsException();
        }

        this.column = column;
    }
}
