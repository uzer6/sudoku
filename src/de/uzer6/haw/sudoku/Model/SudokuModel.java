package de.uzer6.haw.sudoku.Model;

import de.uzer6.haw.sudoku.Util.SudokuUtil;

import java.util.BitSet;

/**
 * Created by lberger on 16/09/16.
 */
public class SudokuModel {
    protected Integer fields[][];

    protected BitSet fieldMask = new BitSet(81);

    public SudokuModel() {
        this.fields = new Integer[9][9];
    }

    public Integer getField(int row, int column) {
        if (this.fields[row] == null || this.fields[row][column] == null) {
            return null;
        }
        return this.fields[row][column];
    }

    public BitSet getFieldMask() {
        return (BitSet) this.fieldMask.clone();
    }

    public boolean isFilled() {
        return (this.fieldMask == SudokuUtil.getInstance().getFilledBoardMask());
    }

    protected void markField(int row, int column, boolean set) {
        this.fieldMask.set((row * 9) + column, set);
    }

    public void setField(Coordinate coordinate, Integer value) throws IndexOutOfBoundsException, IllegalArgumentException {
        this.setField(coordinate.getRow(), coordinate.getColumn(), value);
    }

    public void setField(int row, int column, Integer value) throws IndexOutOfBoundsException, IllegalArgumentException {
        if (row < 0 || row > 8 || column < 0 || column > 8) {
            throw new IndexOutOfBoundsException();
        }
        if (value != null && (value < 1 || value > 9)) {
            throw new IllegalArgumentException();
        }

        this.markField(row, column, (value != null));

        this.fields[row][column] = value;
    }

    protected void setFields(Integer[][] fields) {
        for (int row = 0; row < 9; row++) {
            for (int column = 0; column < 9; column++) {
                Integer val;
                if (fields[row] == null || fields[row][column] == null) {
                    val = null;
                } else {
                    val = new Integer(fields[row][column].intValue());
                }
                this.setField(row, column, val);
            }
        }
        this.fields = fields;
    }

    @Override
    public SudokuModel clone() {
        SudokuModel clone = new SudokuModel();
        clone.setFields(this.fields);

        return clone;
    }
}
