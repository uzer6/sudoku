package de.uzer6.haw.sudoku.Solver;

import de.uzer6.haw.sudoku.Model.Coordinate;
import de.uzer6.haw.sudoku.Model.SudokuModel;
import de.uzer6.haw.sudoku.Solver.Constraint.ConstraintValidator;

import java.util.BitSet;

/**
 * Created by lberger on 17/09/16.
 */
public class SudokuSolver {

    public static SudokuModel getSolvedGrid(SudokuModel base) throws IndexOutOfBoundsException, IllegalArgumentException {
        SudokuModel generated = null;

        while (generated == null) {
            generated = base.clone();
            Coordinate randomField = SudokuSolver.findRandomEmptyField(generated);
            if (randomField != null) {
                generated.setField(randomField, ((int) Math.round(Math.random() * 8)) + 1);
                int constraintViolations = ConstraintValidator.getInstance().getConstraintViolations(generated).size();
                System.out.println(String.format("Set field [row=%d, column=%d], board status changed to [filled=%b, constraintViolations=%d]", randomField.getRow(), randomField.getColumn(), generated.isFilled(), constraintViolations));
                if (false == generated.isFilled() && constraintViolations == 0) {
                    generated = SudokuSolver.getSolvedGrid(generated);
                } else if (constraintViolations != 0) {
                    generated = null;
                }
            } else {
                generated = null;
            }
        }

        return generated;
    }

    public static Coordinate findRandomEmptyField(SudokuModel sudokuModel) throws IndexOutOfBoundsException {
        Coordinate emptyCoordinate = null;

        if (sudokuModel.isFilled()) {
            return emptyCoordinate;
        }

        // Generates a random pivot for empty field search
        int randomPivot = (int) Math.round(Math.random() * 80);
        int start = randomPivot;

        // Determine search direction
        int direction = 1;
        if (randomPivot > 81 / 2) {
            direction = -1;
        }

        BitSet baseMask = sudokuModel.getFieldMask();
        while (emptyCoordinate == null) {
            if (false == baseMask.get(randomPivot)) {
                emptyCoordinate = new Coordinate((randomPivot % 9), (int) Math.floor(randomPivot / 9));
            }

            // Advance pivot
            if (randomPivot == 81) {
                randomPivot = 0;
                direction = 1;
            } else if (randomPivot == 0) {
                randomPivot = 81;
                direction = -1;
            }
            randomPivot += direction;

            // Abort if loop has completed a full cycle
            if (randomPivot == start) {
                break;
            }
        }

        return emptyCoordinate;
    }
}
