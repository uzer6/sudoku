package de.uzer6.haw.sudoku.Solver.Constraint;

import de.uzer6.haw.sudoku.Model.Coordinate;
import de.uzer6.haw.sudoku.Model.SudokuModel;

import java.util.ArrayList;

/**
 * Created by lberger on 17/09/16.
 */
public interface Constraint {

    ArrayList<Coordinate> getConstraintViolations(SudokuModel sudokuModel) throws IndexOutOfBoundsException;
}
