package de.uzer6.haw.sudoku.Solver.Constraint;

import de.uzer6.haw.sudoku.Model.Coordinate;
import de.uzer6.haw.sudoku.Model.SudokuModel;

import java.util.ArrayList;

/**
 * Created by lberger on 17/09/16.
 */
public class RowConstraint implements Constraint {
    @Override
    public ArrayList<Coordinate> getConstraintViolations(SudokuModel sudokuModel) throws IndexOutOfBoundsException {
        ArrayList<Coordinate> violations = new ArrayList<>();

        for (int row = 0; row < 9; row++) {
            boolean validRow = this.isValidRow(sudokuModel, row);

            if (false == validRow) {
                for (int column = 0; column < 9; column++) {
                    violations.add(new Coordinate(row, column));
                }
            }
        }

        return violations;
    }

    public static boolean isValidRow(SudokuModel sudokuModel, int row) {
        boolean[] scratch = new boolean[9];

        for (int column = 0; column < 9; column++) {
            Integer value = sudokuModel.getField(row, column);
            if (value != null) {
                if (scratch[value - 1]) {
                    return false;
                }
                scratch[sudokuModel.getField(row, column) - 1] = true;
            }
        }

        return true;
    }
}
