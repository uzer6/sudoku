package de.uzer6.haw.sudoku.Solver.Constraint;

import de.uzer6.haw.sudoku.Model.Coordinate;
import de.uzer6.haw.sudoku.Model.SudokuModel;

import java.util.ArrayList;

/**
 * Created by lberger on 17/09/16.
 */
public class ConstraintValidator {

    protected ArrayList<Constraint> constraints;

    private static ConstraintValidator instance = new ConstraintValidator();

    public static ConstraintValidator getInstance() {
        return instance;
    }

    private ConstraintValidator() {
        this.constraints = new ArrayList<>();
        this.constraints.add(new RowConstraint());
        this.constraints.add(new ColumnConstraint());
        this.constraints.add(new BlockConstraint());
    }

    /**
     * Matches against all constraints
     * @param sudokuModel
     * @return
     */
    public ArrayList<Coordinate> getConstraintViolations(SudokuModel sudokuModel) throws IndexOutOfBoundsException {
        ArrayList<Coordinate> violations = new ArrayList<>();

        for (Constraint constraint: this.constraints) {
            ArrayList<Coordinate> constraintViolations = constraint.getConstraintViolations(sudokuModel);
            violations.addAll(constraintViolations);
        }

        return violations;
    }
}
