package de.uzer6.haw.sudoku.Solver.Constraint;

import de.uzer6.haw.sudoku.Model.Coordinate;
import de.uzer6.haw.sudoku.Model.SudokuModel;

import java.util.ArrayList;

/**
 * Created by lberger on 17/09/16.
 */
public class BlockConstraint implements Constraint {
    @Override
    public ArrayList<Coordinate> getConstraintViolations(SudokuModel sudokuModel) throws IndexOutOfBoundsException {
        ArrayList<Coordinate> violations = new ArrayList<>();

        for (int boxRow = 0; boxRow < 3; boxRow++) {
            for (int boxColumn = 0; boxColumn < 3; boxColumn++) {
                boolean validBlock = this.isValidBlock(sudokuModel, boxRow, boxColumn);
                if (false == validBlock) {
                    for (int row = 0; row < 3; row++) {
                        for (int column = 0; column < 3; column++) {
                            violations.add(new Coordinate(((boxRow * 3) + row), ((boxColumn * 3) + column)));
                        }
                    }
                }
            }
        }

        return violations;
    }

    /**
     * Validates given box
     * @param sudokuModel
     * @param boxRow
     * @param boxColumn
     * @return boolean
     */
    public static boolean isValidBlock(SudokuModel sudokuModel, int boxRow, int boxColumn) {
        boolean[] scratch = new boolean[9];

        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                Integer value = sudokuModel.getField(((boxRow * 3) + row), (boxColumn * 3) + column);
                if (value != null) {
                    if (scratch[value - 1]) {
                        return false;
                    }

                    scratch[value - 1] = true;
                }
            }
        }

        return true;
    }
}
