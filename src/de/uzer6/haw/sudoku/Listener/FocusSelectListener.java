package de.uzer6.haw.sudoku.Listener;

import javax.swing.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Created by lberger on 16/09/16.
 */
public class FocusSelectListener implements FocusListener {

    protected JTextField input;

    public FocusSelectListener(JTextField input) {
        this.input = input;
    }

    @Override
    public void focusGained(FocusEvent focusEvent) {
        this.input.selectAll();
    }

    @Override
    public void focusLost(FocusEvent focusEvent) {
        this.input.select(0,0);
    }
}
