package de.uzer6.haw.sudoku.Listener;

import de.uzer6.haw.sudoku.Controller.GameController;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by lberger on 16/09/16.
 */
public class KeyboardListener implements KeyListener {

    /**
     * All input Fields
     */
    protected GameController controller;

    /**
     * Current column
     */
    protected int column;

    /**
     * Current row
     */
    protected int row;

    public KeyboardListener(GameController controller, int column, int row) {
        this.controller = controller;
        this.column = column;
        this.row = row;
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {}

    @Override
    public void keyReleased(KeyEvent keyEvent) {}

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_UP) {
            if (this.row > 0) {
                int tmpRow = this.row - 1;
                while (this.controller.getView().getGameView().isLockedField(tmpRow, this.column) && tmpRow > 0) {
                    tmpRow--;
                }
                if (false == this.controller.getView().getGameView().isLockedField(tmpRow, this.column)) {
                    this.controller.getView().getGameView().focusField(tmpRow, this.column);
                }
            }
        } else if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN) {
            if (this.row < 8) {
                int tmpRow = this.row + 1;
                while (this.controller.getView().getGameView().isLockedField(tmpRow, this.column) && tmpRow < 8) {
                    tmpRow++;
                }
                if (false == this.controller.getView().getGameView().isLockedField(tmpRow, this.column)) {
                    this.controller.getView().getGameView().focusField(tmpRow, this.column);
                }
            }
        } else if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT) {
            if (this.column > 0) {
                int tmpColumn = this.column - 1;
                while (this.controller.getView().getGameView().isLockedField(this.row, tmpColumn) && tmpColumn > 0) {
                    tmpColumn--;
                }
                if (false == this.controller.getView().getGameView().isLockedField(this.row, tmpColumn)) {
                    this.controller.getView().getGameView().focusField(row, tmpColumn);
                }
            }
        } else if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (this.column < 8) {
                int tmpColumn = this.column + 1;
                while (this.controller.getView().getGameView().isLockedField(this.row, tmpColumn) && tmpColumn < 8) {
                    tmpColumn++;
                }
                if (false == this.controller.getView().getGameView().isLockedField(this.row, tmpColumn)) {
                    this.controller.getView().getGameView().focusField(this.row, tmpColumn);
                }
            }
        } else if (keyEvent.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.exit(0);
        }
    }
}
