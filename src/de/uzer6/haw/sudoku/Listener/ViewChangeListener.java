package de.uzer6.haw.sudoku.Listener;

import de.uzer6.haw.sudoku.Controller.GameController;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Created by lberger on 16/09/16.
 */
public class ViewChangeListener implements DocumentListener {
    protected GameController controller;

    public ViewChangeListener(GameController controller) {
        this.controller = controller;
    }

    @Override
    public void insertUpdate(DocumentEvent documentEvent) {
        this.controller.onViewChange();
    }

    @Override
    public void removeUpdate(DocumentEvent documentEvent) {
        this.controller.onViewChange();
    }

    @Override
    public void changedUpdate(DocumentEvent documentEvent) {
        this.controller.onViewChange();
    }
}
